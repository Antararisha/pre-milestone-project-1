import { Router } from '@angular/router';
import { AuthService } from 'app/core/auth/auth.service';
// import { user } from './../../../mock-api/common/user/data';
import { ChangeDetectionStrategy, Component, ViewEncapsulation, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { UserService } from 'app/core/user/user.service';
import { User } from 'app/core/user/user.types';

@Component({
    selector: 'profile',
    templateUrl: './profile.component.html',
    encapsulation: ViewEncapsulation.None,
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProfileComponent implements OnInit {
    /**
     * Constructor
     */

    profileUpdate: FormGroup;
    showAlert: boolean = false;
    currentUser: User;
    constructor(
        private _formBuilder: FormBuilder,
        private userService: UserService,
        private AuthService: AuthService,
        private _router: Router

    ) { }
    ngOnInit(): void {
        this.profileUpdate = this._formBuilder.group({
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            name: ['', [Validators.required, Validators.maxLength(15)]],
            email: ['', [Validators.required, Validators.email]],
            address: ['', Validators.required],
            phoneNumber: ['', Validators.required],
            country: ['', Validators.required],
            password: ['', [Validators.required, Validators.minLength(8)]],
        }
        );

    }
    getUseremail() {
        const Email = JSON.parse(localStorage.getItem('user'))['email'];
        this.profileUpdate.get('email').setValue(`${Email}`)

    }
    getUsername() {
        const Name = JSON.parse(localStorage.getItem('user'))['name'];
        this.profileUpdate.get('name').setValue(`${Name}`)

    }
    getpassword() {
        const pass = JSON.parse(localStorage.getItem('user'))['password'];
        this.profileUpdate.get('password').setValue(`${pass}`)
    }


    updateProfile(): void {
        // if ( this.profileUpdate.invalid )
        // {
        //     return;
        // }
        // this.profileUpdate.disable();
        const user = this.profileUpdate.value;
        this.AuthService.updateUser(user)
            .subscribe(
                (response) => {
                    //save JSON return obsject in Local Storage

                    localStorage.setItem('user', JSON.stringify(response));
                    console.log(response);

                    // Navigate to the confirmation required page
                    this._router.navigateByUrl('/posts');
                },

            )
    }


}
