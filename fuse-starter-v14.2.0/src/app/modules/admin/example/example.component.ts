import { PostsService } from '../posts.service';
import { Component, ViewEncapsulation, OnInit } from '@angular/core';


@Component({
    selector: 'example',
    templateUrl: './example.component.html',
    encapsulation: ViewEncapsulation.None
})
export class ExampleComponent implements OnInit {
    public post = [];
    public details = [];
    /**
     * Constructor
     */
    constructor(
        private _postService: PostsService,
    ) { }

    ngOnInit() {

        if (localStorage.getItem('post') != null) {
            this.details = JSON.parse(localStorage.getItem('post'));
            this.details.reverse();
            console.log(this.details);
        }

    }
    sentPost(post) {
        this._postService.sendPost(post);

    }

}


