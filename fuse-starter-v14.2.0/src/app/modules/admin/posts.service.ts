import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of, Subscriber, Subject, observable, BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  private subject = new BehaviorSubject<String>(null);

  public details = [];

  constructor(
    private _httpClient: HttpClient,
  ) { }

  sendPost(post:String){
    this.subject.next(post);
  }

  receivePost():Observable<String>{
   return this.subject.asObservable();
  }

  createPost(post: {
    postdetails: String;
  }): Observable<any> {
    const response = this._httpClient.post('https://jsonplaceholder.typicode.com/posts', post);
    return response;
  }

  showPost(): Observable<any> {
    console.log('dhukse 3');
    if (localStorage.getItem('post') != null) {
      this.details = JSON.parse(localStorage.getItem('post'))['postdetails'];
      console.log(this.details);
    }
    // console.log(post_id);

    return of(this.details);
  }

}