import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DetailsPostComponent } from './details-post.component';
import { DetailsPostRouts } from 'app/modules/admin/details-post/details-post.routing';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatTooltipModule } from '@angular/material/tooltip';
import { FuseCardModule } from '@fuse/components/card';
import { SharedModule } from 'app/shared/shared.module';
import { UpdatePostComponent } from './update-post/update-post.component';


@NgModule({
  declarations: [
    DetailsPostComponent,
    UpdatePostComponent
  ],
  imports: [
    RouterModule,
    RouterModule.forChild(DetailsPostRouts),
    MatButtonModule,
    MatDividerModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatMenuModule,
    MatTooltipModule,
    FuseCardModule,
    SharedModule
  ]
})
export class DetailsPostModule { }
