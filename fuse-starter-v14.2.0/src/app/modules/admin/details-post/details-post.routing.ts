import { Route } from '@angular/router';
import { DetailsPostComponent } from './details-post.component';

export const DetailsPostRouts: Route[] = [
  {
    path: '',
    component: DetailsPostComponent
  }
];