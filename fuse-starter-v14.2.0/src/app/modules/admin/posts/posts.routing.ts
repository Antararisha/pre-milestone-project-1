import { PostsComponent } from './posts.component';
import { Route } from '@angular/router';

export const postsRoutes: Route[] = [
  {
    path: '',
    component: PostsComponent
  }
];