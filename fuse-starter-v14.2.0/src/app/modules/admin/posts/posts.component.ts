import { PostsService } from '../posts.service';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html'
})
export class PostsComponent implements OnInit {
  @ViewChild('createPostsNgForm') createPostsNgForm: NgForm;
  createPosts: FormGroup;
  public index;
  public postText = '';

  constructor(
    private _postService: PostsService,
    private _formBuilder: FormBuilder,
    private _router: Router,
  ) { }

  ngOnInit(): void {
    this.createPosts = this._formBuilder.group({
      postdetails: ['', Validators.required],

    }
    );
  }

 
  createPost(): void {
    const post = this.createPosts.value;
    this._postService.createPost(post)
      .subscribe(
        (response) => {

          var new_post = response;
          //save JSON return obsject in Local Storage
          if (localStorage.getItem('post') == null) {
            localStorage.setItem('post', '[]');
            console.log(post);
            console.log(response);
          }
          var old_post = JSON.parse(localStorage.getItem('post'));
          old_post.push(new_post);
          this.index = old_post.indexOf(new_post);
          console.log("index");
          localStorage.setItem('post', JSON.stringify(old_post));
          // Navigate to the confirmation required page
          this._router.navigateByUrl('/example');
        })

  }
  // detailsPost(){
  //   this.postText = this.details[this.index]

  // }


}

