import { user } from './../../../mock-api/common/user/data';
import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fuseAnimations } from '@fuse/animations';
import { FuseAlertType } from '@fuse/components/alert';
import { AuthService } from 'app/core/auth/auth.service';

@Component({
    selector     : 'auth-sign-up',
    templateUrl  : './sign-up.component.html',
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class AuthSignUpComponent implements OnInit
{
    @ViewChild('signUpNgForm') signUpNgForm: NgForm;

    alert: { type: FuseAlertType; message: string } = {
        type   : 'success',
        message: ''
    };
    signUpForm: FormGroup;
    showAlert: boolean = false;

    /**
     * Constructor
     */
    constructor(
        private _authService: AuthService,
        private _formBuilder: FormBuilder,
        private _router: Router
    )
    {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        // Create the form
        this.signUpForm = this._formBuilder.group({ 
            firstName: ['', Validators.required],
            lastName: ['', Validators.required],
            name: ['', [Validators.required,Validators.maxLength(15)]],
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required,Validators.minLength(8)]],
            // company   : [''],
            agreements: ['', Validators.requiredTrue]
        }
        );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    //Random id generation+username  
    userNameGeneration():void {
        var random_Sting ='';
        var charecters='ASDFGHJKLZXCVBNMQWERTYUIOP#$%&1029384756';
        for (var i=0;i<5;i++){
            random_Sting += charecters.charAt(Math.floor(Math.random()*charecters.length));
            // console.log(random_Sting);

        }
       
       const firstname = `${this.signUpForm.get('firstName').value}`;

       const lastname = `${this.signUpForm.get('lastName').value}`;
       
       const fullName = firstname.concat(lastname.toString());

       const userName = fullName.concat(random_Sting.toString());
       
       this.signUpForm.get('name').setValue(`${userName}`);
       this.signUpForm.get('name');
   }
    /**
     * Sign up
     */
    //  hashCode(str) {
    //     let hash = 0;
    //     for (let i = 0, len = str.length; i < len; i++) {
    //         let chr = str.charCodeAt(i);
    //         hash = (hash << 5) - hash + chr;
    //         hash |= 0; // Convert to 32bit integer
    //     }
    //     console.log(hash);
    //     return hash;
    // }


    signUp(): void
    {
        // Do nothing if the form is invalid
        if ( this.signUpForm.invalid )
        {
            return;
        }

        // Disable the form
        this.signUpForm.disable();

        // Hide the alert
        this.showAlert = false;

        // Sign up

        // let newUser = this.signUpForm.value;
        // newUser.password = this.hashCode(newUser.password);
        const user =this.signUpForm.value;
        this._authService.signUp(user)
            .subscribe(
                (response) => {
                    //save JSON return obsject in Local Storage
                    
                    localStorage.setItem('user',JSON.stringify(response));
                    console.log(response);

                    // Navigate to the confirmation required page
                    this._router.navigateByUrl('/sign-in');
                },
                (response) => {

                    // Re-enable the form
                    this.signUpForm.enable();

                    // Reset the form
                    this.signUpNgForm.resetForm();

                    // Set the alert
                    this.alert = {
                        type   : 'error',
                        message: 'Something went wrong, please try again.'
                    };

                    // Show the alert
                    this.showAlert = true;
                }
            );
    }
}
