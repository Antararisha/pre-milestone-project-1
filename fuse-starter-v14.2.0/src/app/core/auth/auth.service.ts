import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, catchError, observable, Observable, of, switchMap, throwError } from 'rxjs';
import { AuthUtils } from 'app/core/auth/auth.utils';
import { UserService } from 'app/core/user/user.service';
// import { error } from 'console';

@Injectable()
export class AuthService {
    private _authenticated: boolean = false;
    //  isLoggedIn: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(false);

    /**
     * Constructor
     */
    constructor(
        private _httpClient: HttpClient,
        private _userService: UserService
    ) {
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    /**
     * Setter & getter for access token
     */
    set accessToken(token: string) {
        localStorage.setItem('accessToken', token);
    }

    get accessToken(): string {
        return localStorage.getItem('accessToken') ?? '';
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Forgot password
     *
     * @param email
     */
    forgotPassword(email: string): Observable<any> {
        return this._httpClient.post('api/auth/forgot-password', email);
    }

    /**
     * Reset password
     *
     * @param password
     */
    resetPassword(password: string): Observable<any> {
        return this._httpClient.post('api/auth/reset-password', password);
    }

    /**
     * Sign in
     *
     * @param credentials
     */




    signIn(credentials: { email: string, password: string }): Observable<any> {
        // Throw error, if the user is already logged in
        if (this._authenticated) {
            return throwError(() => new Error('user alredy logged in'))

        }
        console.log('dhukse');
        const email = JSON.parse(localStorage.getItem('user'))['email'];
        console.log(email);
        const pass = JSON.parse(localStorage.getItem('user'))['password'];
        console.log(pass);
        if (email === credentials.email && pass === credentials.password) {
            localStorage.setItem('active', 'true');
            this._authenticated = true;
            // this.isLoggedIn.next(true);
            console.log("ajob1");
            return of(true);
        } else {
            return throwError(() => new Error())
        }


        // return this._httpClient.post('https://jsonplaceholder.typicode.com/users', credentials).pipe(
        //     switchMap((response: any) => {
        //         const email = JSON.parse(localStorage.getItem('user'))['email'];
        //         console.log(email);
        //             const pass = JSON.parse(localStorage.getItem('user'))['password'];
        //             console.log(pass);
        //             if(email === credentials.email && pass === credentials.password)
        //             {
        //                 localStorage.setItem('active', 'true');
        //                 this._authenticated = true;
        //                 // this.isLoggedIn.next(true);
        //                 console.log("ajob1");
        //                 return("true");
        //             }Error(response);

        //     })

        // );




    }

    /**
     * Sign out
     */
    signOut(): Observable<any> {
        // Remove the access token from the local storage
        localStorage.removeItem('accessToken');
        localStorage.setItem('active', 'false');

        // Set the authenticated flag to false
        this._authenticated = false;

        // Return the observable
        return of(true);
    }

    /**
     * Sign up
     *
     * @param user
     */
    signUp(user: {
        firstName: String;
        lastName: String;
        name: string;
        email: string;
        password: string;
    }): Observable<any> {
        const response = this._httpClient.post('https://jsonplaceholder.typicode.com/users', user);
        return response;
    }

    /**
     * Unlock session
     *
     * @param credentials
     */
    unlockSession(credentials: { email: string; password: string }): Observable<any> {
        return this._httpClient.post('api/auth/unlock-session', credentials);
    }

    /**
     * Check the authentication status
     */
    check(): Observable<boolean> {
        // Check if the user is logged in
        if (localStorage.getItem('active') === 'true') {
            return of(true);
        }

        // Check the access token availability
        // Check the access token expire date
        return of(false);
        // If the access token exists and it didn't expire, sign in using it
        // return this.signInUsingToken();
    }

    updateUser(user: {
        firstName: String;
        lastName: String;
        name: string;
        email: string;
        password: string;
        address: String;
        phone: Number;
        country: String;
    }): Observable<any> {
        const response = this._httpClient.post('https://jsonplaceholder.typicode.com/users', user);
        return response;
    }
}
